<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>
<body>

    <h1>Buat Account Baru !</h1>

    <form action="welcome" method="POST">
        @csrf

        <b>Sign Up Form</b> <br> <br>
      
        <label for="first Name">First name:</label> <br>
        <input type="text" name="first_name" id="fname" required> <br>
        <label for="last name">Last name:</label> <br>
        <input type="text" name="last_name" id="lname" required> <br> <br>

        <label for="gender">Gender:</label> <br>
        <input type="radio" name="gender" value="male">
        <label for="male">Male</label> <br>
        <input type="radio" name="gender" value="female">
        <label for="female">Female</label> <br>
        <input type="radio" name="gender" value="other">
        <label for="other">Other</label> <br> <br>

        <label for="nationality">Nationality:</label> <br>
        <select name="nationality" id="nationality" required>
            <option value="indonesian">Indonesian</option>
            <option value="british">British</option>
            <option value="japanese">Japanese</option>
            <option value="malaysian">Malaysian</option>
            <option value="american">American</option>
            <option value="other">Other</option>
        </select> <br> <br>

        <label for="language">Language Spoken:</label> <br>
        <input type="checkbox" name="language" value="bahasa indonesia">
        <label for="bahasa indonesia">Bahasa Indonesia</label> <br>
        <input type="checkbox" name="language" value="english">
        <label for="english">English</label> <br>
        <input type="checkbox" name="language" value="other">
        <label for="other">Other</label> <br> <br>

        <label for="bio">Bio:</label> <br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign Up">



    </form>

    
</body>
</html>