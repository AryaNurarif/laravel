<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    /*public function welcome(Request $request) {
        return view('welcome');
    }*/

    public function welcome(Request $request) {
        //dd($request->all());
        $fname = $request->first_name;
        $lname = $request->last_name;

        return view('welcome', compact('fname', 'lname'));
    }
}
